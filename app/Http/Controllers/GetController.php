<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class GetController extends Controller
{
    public function index()
    {
        return [
            'get' => $_GET,
            'post' => $_POST,
            time()
        ];
    }

    public function image01()
    {
        return response()->file(resource_path('image/1826.jpg'));
    }


    public function appjson()
    {
        return [
            'version' => [
                [
                    "name" => "2.1.5版本",
                    "version" => "2.1.5",
                    "build_time"=>'1698582389',
                    "base_version" => "3",
                    "description" => "这是2.1.5版本",
                    "url" => "http://127.0.0.1/electron.zip"
                ],
                [
                    "name" => "2.1.4版本",
                    "version" => "2.1.4",
                    "base_version" => "3",
                    "build_time"=>'1598082389',
                    "description" => "这是2.1.4这是最新版本",
                    "url" => "http://127.0.0.1/electron.zip"
                ]
            ]
        ];
    }
}
