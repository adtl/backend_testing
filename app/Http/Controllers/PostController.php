<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class PostController extends Controller
{
    public function index()
    {
        return [
            'get' => $_GET,
            'post' => $_POST,
            time()
        ];
    }

    public function raw(){
        return [
            'get' => $_GET,
            'post' => $_POST,
            'files' => $_FILES,
            'raw' => file_get_contents("php://input"),
            'header'=>$this->em_getallheaders()
        ];
    }

}
