<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class PatchController extends Controller
{
    public function index()
    {
        return [
            'get' => $_GET,
            'post' => $_POST,
            time(),
            request()->method()
        ];
    }

}
