<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/view', function () {
    return view('view');
});

Route::group(['namespace' => '', 'prefix' => ''], function (\Illuminate\Routing\Router  $Router) {

    $Router->get('get01',[
        \App\Http\Controllers\GetController::class,'index'
    ]);
    $Router->post('post01',[
        \App\Http\Controllers\PostController::class,'index'
    ]);
    $Router->post('post02',[
        \App\Http\Controllers\PostController::class,'index'
    ]);

    $Router->put('put01',[
        \App\Http\Controllers\PutController::class,'index'
    ]);
    $Router->put('putraw',[
        \App\Http\Controllers\PutController::class,'raw'
    ]);
    $Router->post('postraw',[
        \App\Http\Controllers\PostController::class,'raw'
    ]);
    // putraw
    $Router->delete('delete01',[
        \App\Http\Controllers\DeleteController::class,'index'
    ]);

    $Router->patch('patch01',[
        \App\Http\Controllers\PatchController::class,'index'
    ]);
    $Router->get('image01',[
        \App\Http\Controllers\GetController::class,'image01'
    ]);

    $Router->get('app.json',[
        \App\Http\Controllers\GetController::class,'appjson'
    ]);


    // post02 put01 delete01 patch01
});


